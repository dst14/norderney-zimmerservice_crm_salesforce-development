trigger Voucher_B on Voucher__c (before insert, before update, before delete) {
    
    /*
    on insert: initialize code__c of Voucher__c, with a random 7-char code (only A-Z,0-9 are allowed)
    */
    
    if(Trigger.isInsert) {
        
        set<string> newCodes = new set<string>();
        
        for(integer i = 0; i < Trigger.new.size(); i++) {
            while(true) {
                string code = returnCode();
                if(!newCodes.contains(code)) {
                    newCodes.add(code);
                    break;
                }
            }
            
        }
        
        while(true) {
            list<Voucher__c> alreadyExistingVouchers = new list<Voucher__c>([
                SELECT  id,
                        Code__c
                FROM    Voucher__c
                WHERE   Code__c IN: newCodes
            ]);
            if(alreadyExistingVouchers.isEmpty()) {break;} //all codes in newCodes are not used yet
            
            for(Voucher__c voucher : alreadyExistingVouchers) { //remove alreadz used codes from newCodes
                newCodes.remove(voucher.Code__c);
            }
            for(integer i = 0; i < alreadyExistingVouchers.size(); i++) { //create new codes, will be checked again in the loop's next iteration
                while(true) {
                    string code = returnCode();
                    if(!newCodes.contains(code)) {
                        newCodes.add(code);
                        break;
                    }
                }
            }
        }
        
        list<string> newCodesList = new list<string>();
        newCodesList.addAll(newCodes);
        integer i = 0;
        for(Voucher__c voucher : Trigger.new) {
            voucher.Code__c = newCodesList[i];
            i++;
        }
        
    }
    
    string returnCode() {
        string code = '';
        for(integer i = 0; i < 7; i++) {
            integer randomNumber;
            while(true) {
            /*
                randomNumber = integer.valueof(Math.random() * 43);
                randomNumber += 48;
                if(randomNumber>=58 && randomNumber<=64) {continue;}
                break;
            */
                
                // Multiplikator 32 steuert die Länge des möglichen Bandes (hier 25 Zeichen)
                randomNumber = integer.valueof(Math.random() * 25);
                
                // Offset verschiebt auf das erste Zeichen
                randomNumber += 65;
                
                // Unerwünschte Zeichen ausschließen (73 = I, 79 = O)
                if(randomNumber == 73 || randomNumber == 79)
                   {continue;}
                
                break;
            }
            code += String.fromCharArray( new List<integer> { randomNumber } );
        }
        return code;
    }
    
}