public class createVoucherCont {
    
    public Voucher__c voucher {get; set;}
    public Boolean showForm {get; set;}
    
    public createVoucherCont() {
        showForm = true;
        voucher = new Voucher__c();
        
        /*
        //for testing:
        voucher.Amount__c = 10;
        voucher.Salutation__c = 'Herr';
        voucher.LastName__c = 'Adam';
        voucher.Email__c = 'hendrik@die-interaktiven.de';
        voucher.BillingStreet__c = 'street';
        voucher.BillingPostalcode__c = '12345';
        voucher.BillingCity__c = 'city';
        voucher.BillingState__c = 'state';
        voucher.BillingCountry__c = 'country';
        voucher.Format__c = 'PDF';
        */

    }

    public void createVoucher() {
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartObject();
        gen.writeNumberField('amount', voucher.Amount__c);
        gen.writeStringField('salutation', voucher.Salutation__c);
        if(voucher.FirstName__c != null) {
            gen.writeStringField('firstName', voucher.FirstName__c);
        } else {
            gen.writeStringField('firstName', '');
        }
        gen.writeStringField('lastName', voucher.LastName__c );
        gen.writeStringField('email', voucher.Email__c);
        if(voucher.Phone__c != null) {
            gen.writeStringField('phone', voucher.Phone__c);
        } else {
            gen.writeStringField('phone', '');
        }
        gen.writeStringField('billingStreet', voucher.BillingStreet__c);
        gen.writeStringField('billingPostalCode', voucher.BillingPostalCode__c);
        gen.writeStringField('billingCity', voucher.BillingCity__c);
        if(voucher.BillingState__c != null) {
            gen.writeStringField('billingState', voucher.BillingState__c);
        } else {
            gen.writeStringField('billingState', '');
        }
        gen.writeStringField('billingCountry', voucher.BillingCountry__c);
        if(voucher.ShippingName__c != null) {
            gen.writeStringField('shippingName', voucher.ShippingName__c);
        } else {
            gen.writeStringField('shippingName', '');
        }
        if(voucher.ShippingStreet__c != null) {
            gen.writeStringField('shippingStreet', voucher.ShippingStreet__c);
        } else {
            gen.writeStringField('shippingStreet', '');
        }
        if(voucher.ShippingPostalCode__c != null) {
            gen.writeStringField('shippingPostalCode', voucher.ShippingPostalCode__c);
        } else {
            gen.writeStringField('shippingPostalCode', '');
        }
        if(voucher.ShippingCity__c != null) {
            gen.writeStringField('shippingCity', voucher.ShippingCity__c);
        } else {
            gen.writeStringField('shippingCity', '');
        }
        if(voucher.ShippingState__c != null) {
            gen.writeStringField('shippingState', voucher.ShippingState__c);
        } 
        else {
            gen.writeStringField('shippingState', '');
        }
        if(voucher.ShippingCountry__c != null) {
            gen.writeStringField('shippingCountry', voucher.ShippingCountry__c);
        } 
        else {
            gen.writeStringField('shippingCountry', '');
        }
        gen.writeStringField('format', voucher.Format__c);
        gen.writeEndObject();
        
        Http h = new Http(); 
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'application/json'); 
        req.setEndpoint('https://norderney.secure.force.com/services/apexrest/purchaseVoucher'); 
        req.setMethod('POST');
        req.setBody(gen.getAsString());
        HttpResponse res;
        
        if(!Test.isRunningTest()) {
            res = h.send(req);
        }
        
        //string responseBody = res.getBody(); 
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Gutschein wurde erstellt')); // + gen.getAsString()
        showForm = false;
    }    
    
    public PageReference showForm() {
        PageReference newPage = new PageReference(Page.createVoucher.getUrl());
        newPage.setRedirect(true);
        return newPage;
    }
}