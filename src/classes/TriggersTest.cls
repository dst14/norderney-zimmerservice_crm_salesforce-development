@isTest 
private class TriggersTest {

    static testMethod void Voucher_B_test() {
        Voucher__c voucher = new Voucher__c(
            Amount__c = 10,
            Salutation__c = 'Herr',
            LastName__c = 'Last',
            Email__c = 'a@b.c',
            BillingStreet__c = 'street 1',
            BillingPostalCode__c = '12345',
            BillingCity__c = 'city',
            Format__c = 'pdf'
        );
        
        insert voucher;
    }
}