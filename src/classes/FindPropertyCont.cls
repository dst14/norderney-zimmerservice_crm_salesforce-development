global with sharing class FindPropertyCont {
    
    public Case currCase{get; set;}
    public list<Property__c> properties {get; set;} 
    public String searchInput{get; set;} 
    public String templateName{get; set;} 
    public boolean showLabel{get; set;} 
    
    public FindPropertyCont(ApexPages.StandardController stdController)  {
        currCase = (Case)stdController.getRecord();
        
        if(!Test.isRunningTest()) {
            templateName = NorderneyConfig__c.getInstance('CasePropertyEmailTemplate').Value__c;
        } else {
            templateName = 'test1';
        }
        
        showLabel = false;
    }
    
    public void searchProperties() {
        string query = 'SELECT  id, Name ' +
                        'FROM   Property__c ' +
                        'WHERE  Name like \'%' + searchInput + '%\' ' +
                        'LIMIT  1000';
        properties = (list<Property__c>)Database.query(query);
        if(properties.size() > 0) {
            showLabel = false;
        } else {
            showLabel = true;
        }
    }

}