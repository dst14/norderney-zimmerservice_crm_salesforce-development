@isTest
private class DiaUtilitiesTest {

    static testMethod void testObjectTools() {
        String email = 'test@test.de';
        Contact contact = new Contact(
            LastName = 'Salat',
            Email = email 
        );
        
        DiaUtilities.getRecordTypeIdByObjectAndName('ProductMapping__c', 'Hauptartikel');
        
        System.assertEquals(NULL, DiaUtilities.stringValueOrNull(contact, 'FirstName'));
        System.assertEquals(email, DiaUtilities.stringValueOrNull(contact, 'Email'));
        
        System.assertEquals(TRUE, DiaUtilities.isEmpty(contact.Id));
        System.assertEquals(FALSE, DiaUtilities.isEmpty(contact.Email));
    }
    
    static testMethod void testDbTools() {
    	
        DiaUtilities.returnSelectAllQuery('Account');
    }
}