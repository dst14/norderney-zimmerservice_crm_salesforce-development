@isTest 
private class editAddressTest {

    static testMethod void editAddressTest() {
        
        Address__c address = new Address__c(name = 'add1', MainCategory__c = 'Lebensmittel ', SubCategory__c = 'Spezialitäten');
        
        address.MondayMorningInSeasonFrom__c    = '08:00';
        address.MondayMorningInSeasonTo__c      = '12:00';
        address.MondayAfternoonInSeasonFrom__c  = '13:00';
        address.MondayAfternoonInSeasonTo__c    = '17:00';
        address.MondayInSeasonClosed__c         = false;
        
        address.MondayMorningOffSeasonFrom__c    = '08:00';
        address.MondayMorningOffSeasonTo__c      = '12:00';
        address.MondayAfternoonOffSeasonFrom__c  = '13:00';
        address.MondayAfternoonOffSeasonTo__c    = '16:00';
        address.MondayOffSeasonClosed__c         = true;
        
        address.TuesdayMorningInSeasonFrom__c    = '08:00';
        address.TuesdayMorningInSeasonTo__c      = '12:00';
        address.TuesdayAfternoonInSeasonFrom__c  = '13:00';
        address.TuesdayAfternoonInSeasonTo__c    = '17:00';
        address.TuesdayInSeasonClosed__c         = false;
        
        address.TuesdayMorningOffSeasonFrom__c    = '08:00';
        address.TuesdayMorningOffSeasonTo__c      = '12:00';
        address.TuesdayAfternoonOffSeasonFrom__c  = '13:00';
        address.TuesdayAfternoonOffSeasonTo__c    = '16:00';
        address.TuesdayOffSeasonClosed__c         = true;
        
        address.WednesdayMorningInSeasonFrom__c    = '08:00';
        address.WednesdayMorningInSeasonTo__c      = '12:00';
        address.WednesdayAfternoonInSeasonFrom__c  = '13:00';
        address.WednesdayAfternoonInSeasonTo__c    = '17:00';
        address.WednesdayInSeasonClosed__c         = false;
        
        address.WednesdayMorningOffSeasonFrom__c    = '08:00';
        address.WednesdayMorningOffSeasonTo__c      = '12:00';
        address.WednesdayAfternoonOffSeasonFrom__c  = '13:00';
        address.WednesdayAfternoonOffSeasonTo__c    = '16:00';
        address.WednesdayOffSeasonClosed__c         = true;
        
        address.ThursdayMorningInSeasonFrom__c    = '08:00';
        address.ThursdayMorningInSeasonTo__c      = '12:00';
        address.ThursdayAfternoonInSeasonFrom__c  = '13:00';
        address.ThursdayAfternoonInSeasonTo__c    = '17:00';
        address.ThursdayInSeasonClosed__c         = false;
        
        address.ThursdayMorningOffSeasonFrom__c    = '08:00';
        address.ThursdayMorningOffSeasonTo__c      = '12:00';
        address.ThursdayAfternoonOffSeasonFrom__c  = '13:00';
        address.ThursdayAfternoonOffSeasonTo__c    = '16:00';
        address.ThursdayOffSeasonClosed__c         = true;
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController(address);
        
        editAddressCont cont = new editAddressCont(stdCont);
        cont.save();
    }
}