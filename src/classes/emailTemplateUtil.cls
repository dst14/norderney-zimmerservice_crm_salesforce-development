global with sharing class emailTemplateUtil {
	
    //templateName: should be folderDeveloperName/templateDeveloperName
    webService static string getEmailBodyForCaseAndProperty(String templateName, String caseId, String propertyId) {
        string returnMessage;
        
        try {
            string[] templateNameSplit = templateName.split('/');
            EmailTemplate template = [
                SELECT 	body 
                FROM 	EmailTemplate 
                WHERE	DeveloperName =: templateNameSplit[1]
                AND		Folder.developername =: templateNameSplit[0]
            ];
            if(Test.isRunningTest()) {
                template.body = '{!Case.CaseNumber} 123 {!Property__c.Name}';
            }
            
            String Source = template.body;
            
            string caseFieldNames = '';
            string propertyFieldNames = '';
            Integer curPos = 0;
            Pattern mergeFieldPattern = Pattern.compile('\\{![a-zA-Z0-9_\\.]*\\}');
            // (\{\![a-zA-Z._]+.[a-zA-Z._]+\})+  -
            // \\{![a-zA-Z0-9_\\.]*\\} +
            // (\\{!+([a-zA-Z_.]*\\.[a-zA-Z_]*)}) +
            Matcher m = mergeFieldPattern.matcher(Source);
            while(m.find()){
                String mergeField = source.substring(m.start()+2,m.end()-1);
                String[] mergeFieldSplit = mergeField.split('\\.');
                string objectName = mergeFieldSplit.remove(0);
                string fieldName = string.join(mergeFieldSplit, '.');
                if(objectName == 'Case') {
                    caseFieldNames += ','+fieldName;
                }
                if(objectName == 'Property__c') {
                    propertyFieldNames += ','+fieldName;
                }
                curPos = m.end();
            }
            
            /*
            string query = 'SELECT ' + fieldNames + ' FROM ' + objectApiName + ' WHERE id =\'' + recordId + '\'';
            Sobject obj = Database.query(query);
            */
            
            Case currCase = null;
            Property__c property = null;
            string query;
            if(caseFieldNames != '') {
                caseFieldNames = caseFieldNames.substring(1);
                query = 'SELECT ' + caseFieldNames + ' FROM Case WHERE id =\'' + caseId + '\'';
                currCase = (Case)Database.query(query);
            }
            if(propertyFieldNames != '') {
                propertyFieldNames = propertyFieldNames.substring(1);
                query = 'SELECT ' + propertyFieldNames + ' FROM Property__c WHERE id =\'' + propertyId + '\'';
                property = (Property__c)Database.query(query);
            }
            
            
            String s = '';
            curPos = 0;
            mergeFieldPattern = Pattern.compile('\\{![a-zA-Z0-9_\\.]*\\}');
            m = mergeFieldPattern.matcher(Source);
            while(m.find()){
                s += Source.substring(curPos, m.start());
                
                String mergeField = source.substring(m.start()+2,m.end()-1);
                String[] mergeFieldSplit = mergeField.split('\\.');
                string objectName = mergeFieldSplit.remove(0);
                string fieldName = string.join(mergeFieldSplit, '.');
                if(objectName == 'Case') {
                	s += currCase.get(fieldName);
                }
                if(objectName == 'Property__c') {
                	s += property.get(fieldName);
                }
                
                curPos = m.end();
            }
            
            s += source.substring(curPos);
            
            returnMessage = 'OK|'+ s;
            
        } catch (Exception e) {
            returnMessage = 'Error|' + e.getStackTraceString() + ' ' + e.getMessage();
        }
		
        
        return returnMessage;
    }
    
 }