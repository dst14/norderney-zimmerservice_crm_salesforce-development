public class returnSelectAll {

    public static String returnSelectAll(string objectName) {
		Map<String, Schema.SObjectType> objectApiNameToObjectType;
	   	Schema.DescribeSObjectResult objectDescription;
		Map<String, Schema.SObjectField> fieldApiNameToField;
		String sQuery;
		    	
		objectApiNameToObjectType = Schema.getGlobalDescribe(); 
		objectDescription = objectApiNameToObjectType.get(ObjectName).getDescribe();		
		fieldApiNameToField = objectDescription.fields.getMap();
		sQuery = '';
		for (String fieldName : fieldApiNameToField.keySet()) {
			if(sQuery == '') {sQuery = 'SELECT ' + fieldName;}
			else {
				sQuery += ',' + fieldName;
			}
		}
		if(sQuery != '') {sQuery += ',owner.name FROM ' + objectName;}
		
		return sQuery;
	}

}