public class PropertyTriggerHandler extends TriggerHandler {
    
    Map<Id, AhisObject__c> mRealEstate;
    
    
    public override void BeforeUpdate () {
        
        fillNameAndAddress();
    }
    
    public override void BeforeInsert () {
        
        fillNameAndAddress();
    }
    
    public PropertyTriggerHandler() {
        this.mRealEstate = new Map<Id, AhisObject__c>(            
            [SELECT Id, Name, ObjectStreet__c, ObjectCity__c, ObjectPostalCode__c FROM AhisObject__c]);
        
    }
    
    private void fillNameAndAddress () {
        
        List<Property__c> scope = new List<Property__c>();
        
        for (Property__c p : (List<Property__c>) Trigger.new) {
            
            if ((String) p.Name == String.valueOf(p.Id)) {
                scope.add(p);    
            }
            
        }

        for (Property__c p : (List<Property__c>) scope) {
            
            if (this.mRealEstate.containsKey(p.AhisObject__c)) {

                AhisObject__c o = this.mRealEstate.get(p.AhisObject__c);

                p.Name = o.Name;
                p.PropertyPostalCode__c = o.ObjectPostalCode__c;
                p.PropertyStreet__c = o.ObjectStreet__c;
                p.PropertyCity__c = o.ObjectCity__c;

            }       
        }        
    } 

}