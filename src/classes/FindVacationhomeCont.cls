global with sharing class FindVacationhomeCont {
    
    public Case currCase{get; set;}
    public list<VacationHome__c> vacationHomes {get; set;} 
    public String searchInput{get; set;} 
    public String templateName{get; set;} 
    
    public FindVacationhomeCont(ApexPages.StandardController stdController)  {
        templateName = 'Englische_Vorlagen/Test1';
        /*
        if(!Test.isRunningTest()) {
            templateName = AssmannConfig__c.getInstance('CaseSolutionEmailTemplate').Value__c;
        }
		*/
        currCase = (Case)stdController.getRecord();
    }
    
    public void searchVactionHomes() {
        string query = 'SELECT  id, Name ' +
                        'FROM   VacationHome__c ' +
                        'WHERE  Name like \'%' + searchInput + '%\' ' +
                        'LIMIT  1000';
        vacationHomes = (list<VacationHome__c>)Database.query(query);
    }

}