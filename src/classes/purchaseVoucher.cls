@RestResource(urlMapping='/purchaseVoucher/*')

global with sharing class purchaseVoucher {
  
  @HttpPost
    global static Response createVoucher() {
        
        SavePoint sp = Database.setSavepoint();
        
        try {
            
            RestRequest req = RestContext.request;
            Blob body = req.requestBody;
            String bodyString = body.toString();
            
            Voucher__c voucher = (Voucher__c)JSON.deserialize(bodyString,Voucher__c.class);
            
            voucher.Code__c = null;
            voucher.PaymentReference__c = null;
            voucher.PaymentDateTime__c = null;
            voucher.SendDate__c = null;
            voucher.RedeemDateTime__c = null;
            voucher.ShippingState__c = null;
            voucher.Status__c = 'Erzeugt';
            voucher.PurchaseDateTime__c = System.now();

            insert voucher;
            
            Response resp = new Response();
            resp.sfid   = voucher.Id;
            resp.status = 'Success,';
            resp.message  = 'Voucher was created with Salesforce Voucher.Id ' + voucher.Id;
            
            return resp;
        }
        catch(Exception e) {
            
            Response resp = new Response();
            resp.sfid   = null;
            resp.status = 'Failure';
            resp.message  = e.getMessage();
            
            Database.rollback(sp);
            
            return resp;
            
        }
    }
    
    global class Response {
        public String status;
        public String message;
        public String sfid;
    }
}
/*
@RestResource(urlMapping='/purchaseVoucher/*')
global class purchaseVoucher{

    @HttpPost
    global static string doPost(){
        string jsonRequest;
        RestRequest request = RestContext.request;
        jsonRequest = request.requestBody.toString();
        
        Voucher__c voucher = new Voucher__c();
        
        JSONParser parser = JSON.createParser(jsonRequest);
        
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'amount') {
                        voucher.Amount__c = parser.getDoubleValue();
                    } else if (text == 'salutation') {
                        voucher.Salutation__c = parser.getText();
                    } else if (text == 'firstName') {
                        voucher.FirstName__c = parser.getText();
                    } else if (text == 'lastName') {
                        voucher.LastName__c = parser.getText();
                    } else if (text == 'email') {
                        voucher.Email__c = parser.getText();
                    } else if (text == 'phone') {
                        voucher.Phone__c = parser.getText();
                    } else if (text == 'billingStreet') {
                        voucher.BillingStreet__c = parser.getText();
                    } else if (text == 'billingPostalCode') {
                        voucher.BillingPostalCode__c = parser.getText();
                    } else if (text == 'billingCity') {
                        voucher.BillingCity__c = parser.getText();
                    } else if (text == 'billingState') {
                        voucher.BillingState__c = parser.getText();
                    } else if (text == 'billingCountry') {
                        voucher.BillingCountry__c = parser.getText();
                    } else if (text == 'shippingName') {
                        voucher.ShippingName__c = parser.getText();
                    } else if (text == 'shippingStreet') {
                        voucher.ShippingStreet__c = parser.getText();
                    } else if (text == 'shippingPostalCode') {
                        voucher.ShippingPostalCode__c = parser.getText();
                    } else if (text == 'shippingCity') {
                        voucher.ShippingCity__c = parser.getText();
                    } else if (text == 'shippingState') {
                        voucher.ShippingState__c = parser.getText();
                    } else if (text == 'shippingCountry') {
                        voucher.shippingCountry__c = parser.getText();
                    } else if (text == 'format') {
                        voucher.Format__c = parser.getText();
                    } 
                }
            }
        }
        
        insert voucher;
        
        return jsonRequest;
    }
    
}
*/