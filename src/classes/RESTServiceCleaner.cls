@RestResource(urlMapping='/cleaner/*')

global with sharing class RESTServiceCleaner {

    public static String objectName = 'Cleaner__c';    
    
    @HttpGet
    global static RESTResponse get() {
                
        RESTResponse response = new RESTResponse(objectName);
        
        RestRequest request = RestContext.request;
        String uri = request.requestURI.replaceAll('[/]+$', '').trim();

        // Task ID given?
        List<String> uriParts = uri.split('/');                   
        String cleanerId = uriParts.size() == 3 ? uriParts.get(2) : '';
                
        // Get filter parameters
        Map<String,String> parameters = request.params;
        Integer queryLimit    = 0;
        Integer queryPage     = 1;
        Integer queryPageSize = 20;
        String queryWhere     = '';
                
        try {
            
            if (cleanerId.length() > 0) {
                String filteredCleanerId = cleanerId.replaceAll('[^a-zA-Z0-9]+', '').trim();                        
                if (filteredCleanerId != cleanerId) {
                    response.setError('Ungültige ID', 'INVALID_ID');
                    return response;
                }            
                queryWhere = 'Id = \'' + cleanerId + '\'';
            }
			else {
                queryLimit = (parameters.get('limit') != NULL) ? Integer.valueOf(parameters.get('limit').trim()) : queryLimit;
                queryPage = (parameters.get('page') != NULL) ? Integer.valueOf(parameters.get('page').trim()) : queryPage;
                queryPageSize = (parameters.get('perPage') != NULL) ? Integer.valueOf(parameters.get('perPage').trim()) : queryPageSize;
			}
            
            RESTSqlQuery sqlQuery = new RESTSqlQuery(objectName);
            RESTSqlQuery.QueryResult queryResult = sqlQuery.query(
                DiaUtilities.returnAllFieldsAsString(objectName), // fields
                queryWhere,
                'Name ASC',
                queryPageSize,
                queryPage,
                queryLimit
            ); 
            
        	response.addDebug(queryResult.debug);
			response.setObjects(queryResult.objects, queryResult.countAll);
        }
        catch (Exception e) {
            response.setError(e.getMessage() + ' (' + e.getStackTraceString() + ')', 'INTERNAL_ERROR');
        }        
                                                                
        return response;
    }    
}