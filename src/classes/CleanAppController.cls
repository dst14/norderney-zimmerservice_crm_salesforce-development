public class CleanAppController {

    /**
     * Remote action that returns an serialized array of all opened cleaning tasks
     */
    @RemoteAction
    public static String remoteListReceiver(){
        // cleaning task list for select query
        CleaningTask__c [] cleaningTaskList = new List<CleaningTask__c>();

        // query the database
        Date d = Date.today().addDays(1);
        cleaningTaskList = [select Id, Status__c, CleanerFeedback__c, PropertyName__c, NotifyManagement__c, CloseDateTime__c, IsKeyCollectionDone__c from CleaningTask__c where ShowOnBoard__c = true AND TaskDate__c < :d order by Status__c desc];

        // new list with map for assoziative result array
        List<Map<String, String>> lFinalCleaningList = new List<Map<String, String>>();

        // map all list entries into a new map list
        for(CleaningTask__c oCleaningTask : cleaningTaskList){
            Map<String, String> cleaningMap = new Map<String, String>{
                    'id' => oCleaningTask.id,
                    'status' => String.valueOf(oCleaningTask.Status__c == 'open' ? 1 : 0),
                    'feedback' => oCleaningTask.CleanerFeedback__c,
                    'property' => oCleaningTask.PropertyName__c,
                    'notify' => String.valueOf(oCleaningTask.NotifyManagement__c ? 1 : 0),
                    'keyReturned' => String.valueOf(oCleaningTask.IsKeyCollectionDone__c ? 1 : 0),
                    'cleaned' => String.valueOf(oCleaningTask.CloseDateTime__c == null ? 0 : oCleaningTask.CloseDateTime__c.getTime())
                };

            lFinalCleaningList.add(cleaningMap);
        }

        return JSON.serialize(lFinalCleaningList);
    }

}