public with sharing class TaskAssignmentController {
	public TaskAssignmentController() {
		
	}

	public CleaningTask__c[] getUnassignedTasks() {
        return (List<CleaningTask__c>)[SELECT Id, TaskDate__c, Property__r.Name FROM CleaningTask__c WHERE TaskDate__C = TODAY];
    }
    
    public Cleaner__c[] getUserList() {
        return (List<Cleaner__c>)[
            SELECT 	Id, 
            		Name
            FROM 	Cleaner__c
            WHERE 	isActive__c = true
            ORDER BY 	Name ASC];
    }
    
    public String getServiceQueue() {
        Group q = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cleaners' LIMIT 1];
        return q.ID;
    }

}