@RestResource(urlMapping='/auth/*')

global with sharing class RESTServiceAuth {
    
    @HttpGet
    global static RESTResponse get() {
        
        RESTResponse response = new RESTResponse('Cleaner__c');
        
        RestRequest request = RestContext.request;
        String uri = request.requestURI.replaceAll('[/]+$', '').trim();
        
        List<String> uriParts = uri.split('/');                   
        String token = uriParts.size() == 3 ? uriParts.get(2) : '';

        if (token.length() == 0) {
        	response.setError('Kein Token übergeben', 'REQUIRE_TOKEN');
            return response;
        } 
        
        /*
        // Return all fields
        String cleanerFields = DiaUtilities.returnAllFieldsAsString('Cleaner__c');
        String query = 'SELECT ' + cleanerFields + ' FROM Cleaner__c WHERE AppToken__c = \'' + String.escapeSingleQuotes(token) + '\'';
        List<Cleaner__c> cleaners = Database.query(query);
		*/
        List<Cleaner__c> cleaners = [SELECT Id, Name, IsActive__c, AppToken__c FROM Cleaner__c WHERE AppToken__c = :token];
                        
        if (cleaners.size() == 1) {
			response.setObjects(cleaners, 1);
        }
        else {
        	response.setError('Keine Reinigungskraft mit diesem Token gefunden', 'PERMISSION_DENIED');
        }
                        
        return response;
    }

}