@isTest 
private class createVoucherTest {

    static testMethod void createVoucherTest() {
        createVoucherCont cont = new createVoucherCont();
        cont.voucher = new Voucher__c(
            Amount__c = 10,
            Salutation__c = 'Herr',
            LastName__c = 'Last',
            Email__c = 'a@b.c',
            BillingStreet__c = 'street 1',
            BillingPostalCode__c = '12345',
            BillingCity__c = 'city',
            BillingCountry__c = 'DE',
            Format__c = 'pdf'
        );
        cont.createVoucher();
        cont.voucher.FirstName__c = 'string';
        cont.voucher.Phone__c = 'string';
        cont.voucher.BillingState__c = 'string';
        cont.voucher.ShippingName__c = 'string';
        cont.voucher.ShippingStreet__c = 'string';
        cont.voucher.ShippingPostalCode__c = 'string';
        cont.voucher.ShippingCity__c = 'string';
        cont.voucher.ShippingState__c = 'string';
        cont.voucher.ShippingCountry__c = 'string';
        cont.createVoucher();
        cont.showForm();
    }
    
}