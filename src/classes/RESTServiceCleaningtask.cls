@RestResource(urlMapping='/cleaningtask/*')

global with sharing class RESTServiceCleaningtask {

    private static String objectName = 'CleaningTask__c';
    
    private static Map<String,List<String>> returnFields = new Map<String,List<String>>{
        objectName => new List<String>{
            'Assignee__c',
			'BookingCurrent__c',
			'BookingNext__c',
			'CleanerCount__c',
			'CleanerFeedback__c',
			'CloseDateTime__c',
			'ClosingCleaner__c',
			'DepartureDate__c',
			'EstimatedCleaningTime__c',
			'Id',
			'IsKeyCollectionDone__c',
			'LastModifiedDate',
			'Name',
			'NextArrivalDate__c',
			'NotifyManagement__c',
			'Priority__c',
			'PropertyCity__c',
			'PropertyCondition__c',
			'PropertyCountry__c',
			'PropertyName__c',
			'PropertyPostalCode__c',
			'PropertyState__c',
			'PropertyStreet__c',
			'Property__c',
			'RealCleaningTime__c',
			'Specials__c',
			'Status__c',
			'TaskDate__c'
		},
		'Booking__c' => new List<String>{
			'Adult__c',
			'BabyHighChair__c',
			'BabyTravelBed__c',
			'Bathrobe__c',
			'BookingDate__c',
			'ChildAge__c',
			'Childs__c',
			'EndDate__c',
			'Guest__c',
			'HasDog__c',
			'Id',
			'IsKeyCollectionDone__c',
			'LastModifiedDate',
			'Name',
			'Property__c',
			'Specials__c',
			'StartDate__c',
			'Status__c'                
		},
		'Property__c' => new List<String>{
			'AhisId__c',
			'AhisNumber__c',
			'CleaningInfo__c',
			'CleaningTime__c',
			'DetailPage__c',
			'Id',
			'Language__c',
			'LastModifiedDate',
			'Name',
			'PropertyCity__c',
			'PropertyHint__c',
			'PropertyOwner__c',
			'PropertyPostalCode__c',
			'PropertyState__c',
			'PropertyStreet__c',
            'GeoLocation__c'
		},
        'Account' => new List<String>{
            'Id', 'Name', 'Phone'
        }        
    };  
    
    /**
     * POST
     */
    @HttpPost
    global static RESTResponse post() {

        RESTResponse response = new RESTResponse(objectName);        
        
        RestRequest request = RestContext.request;
        String uri = request.requestURI.replaceAll('[/]+$', '').trim();

        // Task ID given?
        List<String> uriParts = uri.split('/');                   
        String taskId = uriParts.size() == 3 ? uriParts.get(2) : '';

		// POST data        
        Blob body = request.requestBody;
        String dataAsJson = body.toString();        
        
        response.addDebug('Request: ' + request);
        
        if (taskId.length() == 0) {
            response.setError('CleaningTask ID wird benötigt. Das Anlegen von neuen Tasks wird über REST nicht unterstützt!', 'ID_REQUIRED');
            return response;
        }
        
        Savepoint eineSchritteSuruck = Database.setSavepoint();
        
        try {
            
            // Find task by requested ID
        	CleaningTask__c task = [SELECT Id FROM CleaningTask__c WHERE Id = :taskId];
            
            if (task == NULL) {
                response.setError('CleaningTask existiert nicht', 'OBJECT_NOT_FOUND');
                return response;                
            }            
            response.addDebug('Send JSON: ' + dataAsJson);
            
            // Convert JSON to CleaningTask__c
        	task = (CleaningTask__c)JSON.deserialize(dataAsJson, CleaningTask__c.class);
            task.Id = taskId;
            
            response.addDebug('Update values: ' + String.valueOf(task));
            update task;
                        
            // Get current cleaner Mappings
            // CHANGES ARE ONLY MADE IF CLEANER IDS WERE SEND!
            while (task.ApiCleanerIds__c != NULL) {
                
                List<Id> cleanerIds = task.ApiCleanerIds__c.split(',');
                Integer countCleaners = cleanerIds.size();
                
                if (countCleaners == 0) {
                	break;
                }
                                        
                response.addDebug('Send cleaner IDs: ' + cleanerIds);                
                Map<Id,Cleaner__c> cleaners = new Map<Id,Cleaner__c>([SELECT Id, Name FROM Cleaner__c WHERE Id IN :cleanerIds]);
                
                if (countCleaners != cleaners.size()) {                    
            		response.setError('Mindestens eine der hinzugefügte Reinigungskräfte existiert nicht', 'NOT_EXISTING_CLEANER');
                	break;   
                }
                
                Map<Id,CleanerMapping__c> cleanerMappings = new Map<Id,CleanerMapping__c>();
                List<CleanerMapping__c> cleanerMappingsToDelete = new List<CleanerMapping__c>();
                List<CleanerMapping__c> cleanerMappingsToInsert = new List<CleanerMapping__c>();
                
				Set<Id> uniqueCleaners = new Set<Id>();

                for (CleanerMapping__c cleanerMapping : [SELECT Cleaner__c, CleaningTask__c FROM CleanerMapping__c WHERE CleaningTask__c = :task.Id]) {
                    
                    Id cleanerId = cleanerMapping.Cleaner__c;
                    // Delete dublettes in mapping table
                    if (uniqueCleaners.contains(cleanerId)) {
                   		cleanerMappingsToDelete.add(cleanerMapping);
                    }
                    else {
                		cleanerMappings.put(cleanerId, cleanerMapping);
                        uniqueCleaners.add(cleanerId);
                    }
                }                                                
                
                // Insert not existing mappings in SF
                for (Cleaner__c cleaner : cleaners.values()) {
                    if (cleanerMappings.containsKey(cleaner.Id) == false) {
                        cleanerMappingsToInsert.add(new CleanerMapping__c(
                            Cleaner__c = cleaner.Id,
                            CleaningTask__c = task.Id
                        ));    
                    }
                }
                if (cleanerMappingsToInsert.size() > 0) {
            		response.addDebug('Added cleaner mappings: ' + cleanerMappingsToInsert);
                    insert cleanerMappingsToInsert;
                }
                
                // Delete not existing mappings from SF
                for (CleanerMapping__c cleanerMapping : cleanerMappings.values()) {                    
                    if (cleaners.containsKey(cleanerMapping.Cleaner__c) == false) {
                        cleanerMappingsToDelete.add(cleanerMapping);
                    }
                }                    
                if (cleanerMappingsToDelete.size() > 0) {
            		response.addDebug('Deleted cleaner mappings: ' + cleanerMappingsToDelete);
                    delete cleanerMappingsToDelete;
                }
                    
                break;
            }
            
            RESTSqlQuery sqlQuery = new RESTSqlQuery(objectName);
            RESTSqlQuery.QueryResult queryResult = sqlQuery.query(
                DiaUtilities.returnAllFieldsAsString(objectName), // fields
                'Id = \'' + task.Id + '\'', 
                '', 1, 1, 0
            );    
            
			response.setObjects(queryResult.objects, 1);            
        }
        catch (Exception e) {
            response.setError(e.getMessage() + ' (' + e.getStackTraceString() + ')', 'INTERNAL_ERROR');
            Database.rollback(eineSchritteSuruck);
        }        
        
        return response;
    }
    
    /**
     * GET
     */
    @HttpGet
    global static RESTResponse get() {
                
        RESTResponse response = new RESTResponse(objectName);
        
        RestRequest request = RestContext.request;
        String uri = request.requestURI.replaceAll('[/]+$', '').trim();

        // Task ID given?
        List<String> uriParts = uri.split('/');                   
        String taskId = uriParts.size() == 3 ? uriParts.get(2) : '';
                
        // Get filter parameters
        Map<String,String> parameters = request.params;
        Integer queryLimit    = 0;
        Integer queryPage     = 1;
        Integer queryPageSize = 20;
        String queryWhere     = '';
        
        try {
            
            if (taskId.length() > 0) {
                String filteredTaskId = taskId.replaceAll('[^a-zA-Z0-9]+', '').trim();                        
                if (filteredTaskId != taskId) {
                    response.setError('Ungültige ID', 'INVALID_ID');
                    return response;
                }            
                queryWhere = 'Id = \'' + taskId + '\'';
            }
            else {     
                
                queryLimit = (parameters.get('limit') != NULL) ? Integer.valueOf(parameters.get('limit').trim()) : queryLimit;
                queryPage = (parameters.get('page') != NULL) ? Integer.valueOf(parameters.get('page').trim()) : queryPage;
                queryPageSize = (parameters.get('perPage') != NULL) ? Integer.valueOf(parameters.get('perPage').trim()) : queryPageSize;
                queryWhere = (parameters.get('where') != NULL) ? parameters.get('where').trim() : '';
                
                // Query where comes as JSON. This will be converted in a CleaningTask__c object to
                // translate the values to allowed ones (e.g. 1 to true).
                // After that the fields will be inspected and an error will be raised if the field
                // does not exists on the object.
                // Then the sql where condition will be build of the given values.
                if (queryWhere.length() > 0) {
                                                        
                    Map<String,String> cleaningTaskMap = (Map<String,String>) JSON.deserialize(queryWhere, Map<String,String>.class);                    
                    List<String> whereConditions = new List<String>();
                    
                    Map<String,Schema.SObjectField> schemaFieldMap  = Schema.SObjectType.CleaningTask__c.fields.getMap();                 
                    Map<String,Object> queryMap = (Map<String,Object>) JSON.deserializeUntyped(queryWhere);
                    response.addDebug('Filter map: ' + queryMap);
                    
                    for (String fieldName : queryMap.keySet()) {
                        
                        Schema.SObjectField fieldSchema = schemaFieldMap.get(fieldName);
                        
                        if (fieldSchema == NULL || cleaningTaskMap.containsKey(fieldName) == FALSE) {
                            response.setError('Ungültiges Feld ' + fieldName, 'INVALID_TASK_ID');
                            return response;
                        }	    
                        
                        Schema.DescribeFieldResult fieldDescribe = fieldSchema.getDescribe();
                        String fieldType = String.valueOf(fieldDescribe.getType());                                                
                        String fieldValue = cleaningTaskMap.get(fieldName);
                        String operator = '=';
                        
                        // Check if an operator is included
                        Pattern myPattern = Pattern.compile('(<|<=|>=|>|<>)::(.+)');
                        Matcher matches = myPattern.matcher(fieldValue);
                        
                        if (matches.find() && matches.group(1) != NULL) {	
                            operator   = matches.group(1);
                            fieldValue = matches.group(2);
                        }                          
                        
                        response.addDebug('Field "' + fieldName + '" is of type "' + fieldType + '" with value: ' + fieldValue + ' (Operator: ' + operator + ')');
                        
                        // Do not quote fields of these types
                        Set<String> noQuoteFields = new Set<String>{'BOOLEAN', 'DOUBLE', 'INTEGER', 'DATE'};                        
                        Boolean noQuotes = noQuoteFields.contains(fieldType);                        
                        
                        fieldValue = noQuotes ? fieldValue : String.escapeSingleQuotes(fieldValue);
                        whereConditions.add(fieldName + ' ' + operator + ' ' + (noQuotes ? '' : '\'') + fieldValue + (noQuotes ? '' : '\''));
                    }		
                    
                    // All are AND linked
                    queryWhere = String.join(whereConditions, ' AND ');
                }
            }
            
            RESTSqlQuery sqlQuery = new RESTSqlQuery(objectName);
            RESTSqlQuery.QueryResult queryResult = sqlQuery.query(
                String.join(returnFields.get(objectName), ','), // fields
                queryWhere,
                'TaskDate__c DESC', // order by
                queryPageSize,
                queryPage,
                queryLimit
            ); 
            
            // Set references to other objects
            Set<Id> bookingIds = new Set<Id>();
            Set<Id> propertyIds = new Set<Id>();
            
            if (queryResult.objects.size() > 0) {            	  
                for (CleaningTask__c cleaningTask : (List<CleaningTask__c>) queryResult.objects) {
                    if (cleaningTask.BookingCurrent__c != NULL) {
                    	bookingIds.add(cleaningTask.BookingCurrent__c);
                    }
                    if (cleaningTask.BookingNext__c != NULL) {
                    	bookingIds.add(cleaningTask.BookingNext__c);                        
                    }
                    if (cleaningTask.Property__c != NULL) {
                    	propertyIds.add(cleaningTask.Property__c);                        
                    }
                }
            }
            
            addReferencesByIds(response, 'Booking__c', bookingIds);
            addReferencesByIds(response, 'Property__c', propertyIds);
            
            if (response.getReferences().containsKey('Booking__c')) {
                Set<Id> guestAccountIds = new Set<Id>();
                for (Booking__c booking : (List<Booking__c>) response.getReferences().get('Booking__c')) {
                    if (booking.Guest__c != NULL) {
                    	guestAccountIds.add(booking.Guest__c);
                    }
                }
            	addReferencesByIds(response, 'Account', guestAccountIds);
            }
            
        	response.addDebug(queryResult.debug);
			response.setObjects(queryResult.objects, queryResult.countAll, queryPageSize, queryPage);
        }
        catch (Exception e) {
            response.setError(e.getMessage() + ' (' + e.getStackTraceString() + ')', 'INTERNAL_ERROR');
        }        
                                                                
        return response;
    }   
    
    private static void addReferencesByIds(RESTResponse response, String name, Set<Id> ids) {
              
        if (ids.size() == 0) {
            return;
        }
        
        String fieldsToReturn; 
            
        if (returnFields.containsKey(name) == true) {
            fieldsToReturn = String.join(returnFields.get(name), ',');
        }
        else {
            fieldsToReturn = DiaUtilities.returnAllFieldsAsString(name);
        }
        
        List<SObject> objects = Database.query('SELECT ' + fieldsToReturn + ' FROM ' + name + ' WHERE Id IN :ids');                                
        
        if (objects.size() > 0) {
        	response.addReferences(name, objects);
        }        
    }
}