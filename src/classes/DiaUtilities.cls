global class DiaUtilities {

    public static String stringValueOrNull(SObject objectToAsk, String property) {    
        if (objectToAsk.get(property) != NULL) {
            return String.valueOf(objectToAsk.get(property));
        }        
        return NULL;
    }
    
    public static Boolean isEmpty(Id id) {
        return (id == NULL || String.valueOf(id).length() == 0);     
    }
    
    public static Boolean isEmpty(String str) {
        return (str == NULL || str.length() == 0);
    }
    
    public static Id getRecordTypeIdByObjectAndName(String objectName, String name) {
        try {
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType = :objectName and Name = :name LIMIT 1];
            System.debug(rt);
        	return rt.Id;
        } 
        catch (Exception e) {
            system.debug('DIA diaUtilities.getRecordTypeIdByObjectAndName exception: ' + e);
            return NULL;
        }
    }
    
    public static List<String> returnAllFieldsAsList(String objectName) {
        
        try {
	        List<String> fieldNames = new List<String>();
    	    Map<String,Schema.SObjectType> objectApiNameToObjectType;
			Map<String, Schema.SObjectField> fieldApiNameToField;
			Schema.DescribeSObjectResult objectDescription;
    	    String sQuery;
        	
			objectApiNameToObjectType = Schema.getGlobalDescribe();
    	    
			objectDescription = objectApiNameToObjectType.get(ObjectName).getDescribe();
	        
			fieldApiNameToField = objectDescription.fields.getMap();
			
	        sQuery = '';
    	    
			for (String fieldName : fieldApiNameToField.keySet()) {
            
				fieldNames.add(fieldName);
			}
		
			return fieldNames;
        }
        catch(Exception e) {
            
            system.debug('DIA exception: ' + e);
            
            return null;
        }
    }
    
    public static String returnAllFieldsAsString(String objectName) {
        
        List<String> fieldNames = returnAllFieldsAsList(objectName);
        
        if(fieldNames != null && fieldNames.size() > 0) {
            return String.join(fieldNames, ', ');
        }
        else {
            return '';
        }
    }
    
	public static String returnSelectAllQuery(String objectName) {
        
        String fieldNames = returnAllFieldsAsString(objectName);
        
        if(fieldNames != null) {
            return 'SELECT ' + fieldNames + ' FROM ' + objectName;
        }
        else {
            return '';
        }
    }
}