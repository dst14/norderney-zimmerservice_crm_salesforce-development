@isTest 
private class REST_Vouchers_Test {
    
    static testMethod void REST_Vouchers_Test() {
        
        Voucher__c voucher = new Voucher__c(
            Amount__c            = 200,
            FirstName__c         = 'Bogus',
            LastName__c          = 'Voidberg', 
            Email__c             = 'bogus@bogusman.com', 
            BillingStreet__c     = 'Street 1', 
            BillingPostalCode__c = '12345', 
            BillingCity__c       = 'Voidcity'	
        );
            
        RestRequest req = new RestRequest();
        req.requestBody = Blob.valueof(JSON.serialize(voucher));
        RestContext.request = req;
        REST_Vouchers.Response response;
        
        // First is insert
        response = REST_Vouchers.upsertVoucher(); 
        System.debug(response);
        System.assertNotEquals(null, response.voucher.Id, 'Voucher Id expected');
        
        Id voucherId = response.voucher.Id;
        
        // Than update should work
        req.requestBody = Blob.valueof('{"Id": "' + voucherId + '", "Status__c": "paid"}');
        RestContext.request = req;
        response = REST_Vouchers.upsertVoucher();        
        System.assertEquals('OK', response.status, 'Update should work');
        
        // check if shipping address does exist
        String shippingStreet = [SELECT ShippingStreet__c FROM Voucher__c WHERE Id = :voucherId].ShippingStreet__c;
        System.assertEquals(voucher.BillingStreet__c, shippingStreet);
        
        // After Status__c has been set to other than created 
        // update should work no more
        req.requestBody = Blob.valueof('{"Id": "' + voucherId + '", "Amount__c": 2000}');
        RestContext.request = req;
        response = REST_Vouchers.upsertVoucher();        
        System.assertEquals('NOK', response.status, 'Update should not work after Status__c has been set to other than "created"');
     }    
}