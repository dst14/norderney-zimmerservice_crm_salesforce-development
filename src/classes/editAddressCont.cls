public class editAddressCont {
    
    public Address__c address {get; set;}
    public string currUserName {get; set;}
    
    public class dayRow {
        public string dayName {get; set;}
        //public string dayNumber {get; set;}
        public Address__c dummyAddress {get; set;} //used to have time fields
    }
    public list<dayRow> dayRows {get; set;}
    
    public editAddressCont (ApexPages.StandardController stdController)  {
        address = (Address__c)stdController.getRecord();
        
        if(address.id != null) {
            string query = returnSelectAll.returnSelectAll('Address__c');
            list<Address__c> addresses = Database.query(query + ' WHERE Id = \''+ address.id + '\'');
            address = addresses[0];
        } else {
            currUserName = System.UserInfo.getName();
        }
        
        dayRows = new list<dayRow>();
        for(integer i = 1; i <= 7; i++) {
            dayRow dayRow = new dayRow();
            dayRow.dummyAddress = new Address__c();
            //dayRow.dayNumber = i;
            if(i == 1) {
                dayRow.dayName = 'Montag';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.MondayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.MondayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.MondayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.MondayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.MondayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.MondayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.MondayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.MondayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.MondayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.MondayOffSeasonClosed__c;
            }
            else if(i == 2) {
                dayRow.dayName = 'Dienstag';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.TuesdayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.TuesdayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.TuesdayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.TuesdayAfternoonInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.TuesdayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.TuesdayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.TuesdayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.TuesdayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.TuesdayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.TuesdayOffSeasonClosed__c;
            }
            else if(i == 3) {
                dayRow.dayName = 'Mittwoch';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.WednesdayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.WednesdayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.WednesdayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.WednesdayAfternoonInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.WednesdayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.WednesdayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.WednesdayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.WednesdayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.WednesdayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.WednesdayOffSeasonClosed__c;
            }
            else if(i == 4) {
                dayRow.dayName = 'Donnerstag';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.ThursdayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.ThursdayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.ThursdayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.ThursdayAfternoonInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.ThursdayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.ThursdayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.ThursdayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.ThursdayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.ThursdayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.ThursdayOffSeasonClosed__c;
            }
            else if(i == 5) {
                dayRow.dayName = 'Freitag';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.FridayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.FridayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.FridayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.FridayAfternoonInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.FridayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.FridayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.FridayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.FridayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.FridayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.FridayOffSeasonClosed__c;
            }
            else if(i == 6) {
                dayRow.dayName = 'Samstag';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.SaturdayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.SaturdayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.SaturdayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.SaturdayAfternoonInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.SaturdayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.SaturdayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.SaturdayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.SaturdayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.SaturdayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.SaturdayOffSeasonClosed__c;
            }
            else if(i == 7) {
                dayRow.dayName = 'Sonntag';
                dayRow.dummyAddress.MondayMorningInSeasonFrom__c    = address.SundayMorningInSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningInSeasonTo__c      = address.SundayMorningInSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c  = address.SundayAfternoonInSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonInSeasonTo__c    = address.SundayAfternoonInSeasonTo__c;
                dayRow.dummyAddress.MondayInSeasonClosed__c         = address.SundayInSeasonClosed__c;
                
                dayRow.dummyAddress.MondayMorningOffSeasonFrom__c    = address.SundayMorningOffSeasonFrom__c;
                dayRow.dummyAddress.MondayMorningOffSeasonTo__c      = address.SundayMorningOffSeasonTo__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c  = address.SundayAfternoonOffSeasonFrom__c;
                dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c    = address.SundayAfternoonOffSeasonTo__c;
                dayRow.dummyAddress.MondayOffSeasonClosed__c         = address.SundayOffSeasonClosed__c;
            }
            
            dayRows.add(dayRow);
        }
    }
    
    public pageReference save() {
        boolean isError = false;
        string daysWithError = '';
        for(dayRow dayRow : dayRows) {
            /*
            if(
               (dayRow.dummyAddress.Montag_Vormittag_von__c != null && dayRow.dummyAddress.Montag_Vormittag_bis__c == null && dayRow.dummyAddress.Montag_Nachmittag_bis__c == null)
               ||
               (dayRow.dummyAddress.Montag_Vormittag_von__c == null && dayRow.dummyAddress.Montag_Vormittag_bis__c != null)
               ||
               (dayRow.dummyAddress.Montag_Nachmittag_von__c == null && dayRow.dummyAddress.Montag_Nachmittag_bis__c != null)
              )
            */
            if(false) {
                isError = true;
                if(daysWithError != '') {daysWithError += ',';}
                daysWithError += dayRow.dayName;
            }
            else {
                
                if(dayRow.dayName == 'Montag') {
                    address.MondayMorningInSeasonFrom__c    = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.MondayMorningInSeasonTo__c      = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.MondayAfternoonInSeasonFrom__c  = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.MondayAfternoonInSeasonTo__c    = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.MondayInSeasonClosed__c         = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.MondayMorningOffSeasonFrom__c   = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.MondayMorningOffSeasonTo__c     = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.MondayAfternoonOffSeasonFrom__c = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.MondayAfternoonOffSeasonTo__c   = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.MondayOffSeasonClosed__c        = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
                if(dayRow.dayName == 'Dienstag') {
                    address.TuesdayMorningInSeasonFrom__c       = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.TuesdayMorningInSeasonTo__c         = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.TuesdayAfternoonInSeasonFrom__c     = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.TuesdayAfternoonInSeasonTo__c       = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.TuesdayInSeasonClosed__c            = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.TuesdayMorningOffSeasonFrom__c      = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.TuesdayMorningOffSeasonTo__c        = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.TuesdayAfternoonOffSeasonFrom__c    = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.TuesdayAfternoonOffSeasonTo__c      = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.TuesdayOffSeasonClosed__c           = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
                if(dayRow.dayName == 'Mittwoch') {
                    address.WednesdayMorningInSeasonFrom__c     = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.WednesdayMorningInSeasonTo__c       = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.WednesdayAfternoonInSeasonFrom__c   = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.WednesdayAfternoonInSeasonTo__c     = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.WednesdayInSeasonClosed__c          = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.WednesdayMorningOffSeasonFrom__c    = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.WednesdayMorningOffSeasonTo__c      = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.WednesdayAfternoonOffSeasonFrom__c  = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.WednesdayAfternoonOffSeasonTo__c    = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.WednesdayOffSeasonClosed__c         = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
                if(dayRow.dayName == 'Donnerstag') {
                    address.ThursdayMorningInSeasonFrom__c      = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.ThursdayMorningInSeasonTo__c        = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.ThursdayAfternoonInSeasonFrom__c    = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.ThursdayAfternoonInSeasonTo__c      = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.ThursdayInSeasonClosed__c           = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.ThursdayMorningOffSeasonFrom__c     = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.ThursdayMorningOffSeasonTo__c       = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.ThursdayAfternoonOffSeasonFrom__c   = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.ThursdayAfternoonOffSeasonTo__c     = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.ThursdayOffSeasonClosed__c          = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
                if(dayRow.dayName == 'Freitag') {
                    address.FridayMorningInSeasonFrom__c        = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.FridayMorningInSeasonTo__c          = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.FridayAfternoonInSeasonFrom__c      = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.FridayAfternoonInSeasonTo__c        = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.FridayInSeasonClosed__c             = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.FridayMorningOffSeasonFrom__c       = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.FridayMorningOffSeasonTo__c         = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.FridayAfternoonOffSeasonFrom__c     = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.FridayAfternoonOffSeasonTo__c       = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.FridayOffSeasonClosed__c            = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
                if(dayRow.dayName == 'Samstag') {
                    address.SaturdayMorningInSeasonFrom__c      = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.SaturdayMorningInSeasonTo__c        = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.SaturdayAfternoonInSeasonFrom__c    = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.SaturdayAfternoonInSeasonTo__c      = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.SaturdayInSeasonClosed__c           = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.SaturdayMorningOffSeasonFrom__c     = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.SaturdayMorningOffSeasonTo__c       = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.SaturdayAfternoonOffSeasonFrom__c   = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.SaturdayAfternoonOffSeasonTo__c     = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.SaturdayOffSeasonClosed__c          = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
                if(dayRow.dayName == 'Sonntag') {
                    address.SundayMorningInSeasonFrom__c    = dayRow.dummyAddress.MondayMorningInSeasonFrom__c;
                    address.SundayMorningInSeasonTo__c      = dayRow.dummyAddress.MondayMorningInSeasonTo__c;
                    address.SundayAfternoonInSeasonFrom__c  = dayRow.dummyAddress.MondayAfternoonInSeasonFrom__c;
                    address.SundayAfternoonInSeasonTo__c    = dayRow.dummyAddress.MondayAfternoonInSeasonTo__c;
                    address.SundayInSeasonClosed__c         = dayRow.dummyAddress.MondayInSeasonClosed__c;
                    
                    address.SundayMorningOffSeasonFrom__c   = dayRow.dummyAddress.MondayMorningOffSeasonFrom__c;
                    address.SundayMorningOffSeasonTo__c     = dayRow.dummyAddress.MondayMorningOffSeasonTo__c;
                    address.SundayAfternoonOffSeasonFrom__c = dayRow.dummyAddress.MondayAfternoonOffSeasonFrom__c;
                    address.SundayAfternoonOffSeasonTo__c   = dayRow.dummyAddress.MondayAfternoonOffSeasonTo__c;
                    address.SundayOffSeasonClosed__c        = dayRow.dummyAddress.MondayOffSeasonClosed__c;
                }
            }
        }
        
        if(!isError) {
                upsert address;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Falsche Öffnungszeiten für: ' + daysWithError));
                        return null;
        }
        
        return new pageReference('/' + address.id);
    }
}