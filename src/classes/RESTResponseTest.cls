@isTest
private class RESTResponseTest {

    static testMethod void createResponse() {
    
        List<Contact> contacts = new List<Contact>();        
        contacts.add(new Contact(LastName = 'Test1'));
        contacts.add(new Contact(LastName = 'Test2'));        
        
        RESTResponse response = new RESTResponse('Contact');

        //response.addObjects(contacts, 'contacts', contacts.size());
        response.addReferences('contact', contacts);
        Map<String, List<sObject>> lObj = response.getReferences();
        response.setObjects(contacts);
		response.setObjects(contacts, 10);
        response.setObjects(contacts, 10, 2, 2);
        response.addDebug('Debug Test');
        response.setError('Error message', 'ERROR_CODE');
        
        //RESTResponse.Error e = response.getError();
                                                   
        System.assertEquals(response.getStatus(), 'NOK');    
    }
}