@isTest(SeeAllData=true)
private class emailTemplateUtilTest {
    static testMethod void emailTemplateUtilTest() {
        
        Property__c property = new Property__c(name = 'property1');
        insert property;
        
        Case c = new Case(Status = 'neu', Origin = 'Email');
        insert c;
        
        Test.startTest();
        emailTemplateUtil.getEmailBodyForCaseAndProperty(NorderneyConfig__c.getInstance('CasePropertyEmailTemplate').Value__c, c.id, property.id);
        Test.stopTest();
    }
    
}