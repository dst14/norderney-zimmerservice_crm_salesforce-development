@RestResource(urlMapping='/vouchers/*')

global with sharing class REST_Vouchers {
    
    @HttpPost
    global static Response upsertVoucher() {
        
        Response response = new Response();
        SavePoint savepoint = Database.setSavepoint();

		response.status = 'OK';
            
        try {            
            RestRequest request = RestContext.request;
            Blob body = request.requestBody;
            String bodyString = body.toString();
            
            Voucher__c voucher = (Voucher__c)JSON.deserialize(bodyString, Voucher__c.class);
            Voucher__c existingVoucher;
            
            // Transfer billing to shipping address if none given and BillingStreet is given
            // ATTENTION: Sending HasShippingAddress__c without BillingStreet__c will left
            // the shipping address untouched as HasShippingAddress__c is also false if not sent.
            if (!voucher.HasShippingAddress__c && voucher.BillingStreet__c != null) {
                voucher.ShippingName__c       = (voucher.FirstName__c + ' ' + voucher.LastName__c).trim();
                voucher.ShippingStreet__c     = voucher.BillingStreet__c;
                voucher.ShippingPostalCode__c = voucher.BillingPostalCode__c;
                voucher.ShippingCity__c       = voucher.BillingCity__c;
                voucher.ShippingState__c      = voucher.BillingState__c;
                voucher.ShippingCountry__c    = voucher.BillingCountry__c;
            }
            
            if (voucher.Id == null) {    
                
                voucher.Code__c = null;
                voucher.PaymentReference__c = null;
                voucher.PaymentDateTime__c = null;
                voucher.SendDate__c = null;
                voucher.RedeemDateTime__c = null;
                voucher.ShippingState__c = null;
                
                // If payment method is invoice on insert the 
                // voucher cannot be edited no more after that!
                voucher.Status__c = (voucher.PaymentMethod__c == 'invoice') ? 'pendingPayment' : 'created';
                voucher.PurchaseDateTime__c = System.now();
                insert voucher;
                
                // Get created code
                Voucher__c voucherInserted = [SELECT Code__c FROM Voucher__c WHERE Id = :voucher.Id];            
                voucher.Code__c = voucherInserted.Code__c;
                
                response.message  = 'Voucher was created with Id ' + voucher.Id;                
            }
            else {

                Boolean isEditable = [SELECT IsOrderEditable__c FROM Voucher__c WHERE Id = :voucher.Id].IsOrderEditable__c;
                
                if (!isEditable) {
               		throw new REST_Vouchers_Exception('Gutschein kann nicht mehr bearbeitet werden'); 
                }
                
                update voucher;
                
                response.message  = 'Voucher was updated';
            }
            
            response.voucher = voucher;            
        }
        catch (Exception e) {            
            Database.rollback(savepoint);
            response.status = 'NOK';
            response.message  = e.getMessage();
        }
        
        return response;
    }
    
    global class Response {
        public String status;
        public String message;
        public Voucher__c voucher;
    }
    
	public class REST_Vouchers_Exception extends Exception {}     
}