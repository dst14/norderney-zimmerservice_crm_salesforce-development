global class RESTSqlQuery {

    public static final Integer MAX_PAGE_SIZE = 500;
    
    private String objectName;
    
    public RESTSqlQuery(String objectName) {
        this.objectName = objectName;
    }
    
    public QueryResult query(String sqlSelect, String sqlWhere, String sqlOrderBy, Integer pageSize, Integer pageNumber, Integer queryLimit) {

        String sql = 'SELECT #select# FROM ' + objectName;
                       
        sqlOrderBy = sqlOrderBy.length() > 0 ? sqlOrderBy : '';
        pageSize   = pageSize > 0 && pageSize <= MAX_PAGE_SIZE ? pageSize : MAX_PAGE_SIZE;
        pageNumber = pageNumber > 0 ? pageNumber : 0;
        queryLimit= (queryLimit > 0 && queryLimit <= 50000) ? queryLimit : 50000; // 50.000 is Salesforce limit
        
        QueryResult result = new QueryResult();
        
        while (TRUE) {
        
            //------------------------
            // locator                        
            //------------------------                          
            String sqlLocator = 
                sql + 
                (sqlWhere.length() > 0 ? ' WHERE ' + sqlWhere : '') + 
                (sqlOrderBy.length() > 0 ? ' ORDER BY ' + sqlOrderBy : '') + ' LIMIT ' + queryLimit;
                
            sqlLocator = sqlLocator.replaceFirst('#select#', 'Id');        
            List<SObject> orderLocator = Database.query(sqlLocator);
            List<SObject> orders = new List<SObject>();
            Boolean hasMore = FALSE;
            Integer locatorOffset = 0;            
            
            result.countAll = orderLocator.size();
            
            System.debug('countAll: ' + orderLocator.size());
                    
            List<Id> ids = new List<Id>();
            
            if (orderLocator.size() > 0) {
            
                locatorOffset = (pageNumber * pageSize) - pageSize;
                locatorOffset = locatorOffset < 0 ? 0 : locatorOffset;
                
                if (locatorOffset > orderLocator.size()-1) {
                    result.error = 'Offset ' + (locatorOffset+1) + ' exceeds result size of ' + orderLocator.size();
                    break;
                }
                        
                Integer i;
                for (i = locatorOffset; (i <= locatorOffset + (pageSize-1) && i <= orderLocator.size()-1) ; i++) {
                    ids.add(orderLocator.get(i).Id);
                }
                hasMore = (i < orderLocator.size());
                
                System.debug('hasMore: ' + i + ' < ' + (orderLocator.size()));
                System.debug('locatorOffset: ' + locatorOffset);
                                  
                //------------------------
                // final sql
                //------------------------              
                sql = sql.replaceFirst('#select#', sqlSelect);
                
                if (ids.size() > 0) {
                
                    sql += ' WHERE Id IN :ids';
                    
                    if (sqlWhere.length() > 0) {
                        sql += ' AND ' + sqlWhere;
                    }
                    
                    if (sqlOrderBy.length() > 0) {
                        sql += ' ORDER BY ' + sqlOrderBy;
                    }
                    
                    orders = Database.query(sql);
                }
                
            	System.debug('IDs: ' + ids);
            }
            
            System.debug('SOQL: ' + sql);
                                       
            result.objects = orders;
            result.hasMore = hasMore;
            result.debug  = 'locator SOQL: ' + sqlLocator + ',\n';
            result.debug += 'final SOQL: ' + sql + ',\n';
            result.debug += 'countAll: ' + result.countAll + ',\n';
            result.debug += 'offset: ' + locatorOffset + ',\n';
            result.debug += 'pageSize: ' + pageSize + ',\n';
            result.debug += 'count IDs: ' + ids.size();            
            
            break;
        }
        
        return result;
    }
    
    global class QueryResult {        
        public List<Sobject> objects = new List<SObject>();
        public Boolean hasMore = FALSE;
        public String debug = '';
        public Integer countAll;
        public String error = '';
    }
}