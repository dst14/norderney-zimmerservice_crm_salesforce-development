@isTest
private class RESTService_Tests {
    
    @isTest static void testRESTServiceCleaner() {

        // Create test data
        NZITestDataFactory.objectsToCreate.add('Cleaner__c');
        NZITestDataFactory.testCreateData();
        
        // Create REST request
        RestRequest request = new RestRequest();
        request.requestURI = '/cleaner/';
        request.httpMethod = 'GET';
                
        RestContext.request  = request;
        
        Test.startTest();
        
        // All cleaners (though paginated by default)
		RESTResponse response = RESTServiceCleaner.get();        
        System.debug('RESTService_Tests::testRESTServiceCleaner() - all cleaners reponse: ' + response);
        System.assert(response.getObjects().size() > 0, 'Expect found cleaners');
        
        // Certain cleaner
        Id testCleanerId = NZITestDataFactory.allCleaners.get(0).Id;
        request.requestURI = '/cleaner/' + testCleanerId;
		response = RESTServiceCleaner.get();        
        System.debug('RESTService_Tests::testRESTServiceCleaner() - query certain cleaner reponse: ' + response);
        System.assert(response.getObjects().get(0).Id == testCleanerId, 'Expect cleaner to match requested one');
        
		Test.stopTest();        
    }
}