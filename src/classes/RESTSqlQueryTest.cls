@isTest
private class RESTSqlQueryTest {

    static testMethod void getUser() {
        
        List<Contact> contacts = new List<Contact>();
                
        contacts.add(new Contact(
            Salutation = 'Herr',
            FirstName = 'Donald',
            LastName = 'Duck',
            Email = 'donald@duck.de'
        ));
        contacts.add(new Contact(
            Salutation = 'Frau',
            FirstName = 'Daisy',
            LastName = 'Duck',
            Email = 'daisy@duck.de'
        ));
        contacts.add(new Contact(
            Salutation = 'Herr',
            FirstName = 'Mickey',
            LastName = 'Maus',
            Email = 'mickey@maus.de'
        ));
        contacts.add(new Contact(
            Salutation = 'Herr',
            FirstName = 'Dagobert',
            LastName = 'Duck',
            Email = 'dagobert@duck.de'
        ));
        contacts.add(new Contact(
            Salutation = 'Herr',
            FirstName = 'Daniel',
            LastName = 'Düsentrieb',
            Email = 'daniel@duesentrieb.de'
        ));
        contacts.add(new Contact(
            Salutation = 'Herr',
            FirstName = 'Gustav',
            LastName = 'Gans',
            Email = 'gustav@gans.de'
        ));
        contacts.add(new Contact(
            Salutation = 'Herr',
            FirstName = 'Mini',
            LastName = 'Maus',
            Email = 'mini@maus.de'
        ));

        insert contacts;
        
        Integer pageSize = 2;
        Integer dbLimit = 100;
        
        RESTSqlQuery sqlQuery = new RESTSqlQuery('Contact');
        RESTSqlQuery.QueryResult result = sqlQuery.query(
            'Id, FirstName, LastName', 
            'Email LIKE \'%@duck.de\'', 
            '', // orderSql
            pageSize,
            1,
        	dbLimit); // pageNumber
        System.debug('Result: debug: ' + result.debug);
            
        List<SObject> objects = result.objects;
        
        // assertions
        System.assertEquals(TRUE, result.hasMore);
        System.assertEquals(3, result.countAll);
        System.assertEquals(pageSize, objects.size());
        
                
        // new pageSize
        pageSize = 0;
        
        result = sqlQuery.query(
            'Id, FirstName, LastName', 
            'Email LIKE \'%@duck.de\'', 
            '', // orderSql
            pageSize,
            0,
        	dbLimit); // pageNumber
        System.debug('Result: debug: ' + result.debug);        
        
        // assertions
        System.assertEquals(FALSE, result.hasMore);
        System.assertEquals(3, result.countAll);        
   }
}