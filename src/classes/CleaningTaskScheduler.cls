public class CleaningTaskScheduler implements 
Database.Batchable<SObject>, 
Database.AllowsCallouts, 
Database.Stateful,
Schedulable  {
    
    String sQuery;
    String errMessage;
    
    public CleaningTaskScheduler () {
        
    }
    
    // schedulable
    public void execute(SchedulableContext SC) {}
    
    // batchable
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(sQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
    }
    
    public void finish(Database.BatchableContext BC) {
        Messaging.Singleemailmessage msg;
        String sSubject;
        
        if(errMessage != '' && !Test.isRunningTest()) {
            msg = new Messaging.Singleemailmessage();
            sSubject = 'Job processOrdersWSlots (' + System.now().format() + ') finished';
            msg.setSubject(sSubject);
            msg.setToAddresses(new String[] {'genius@kunden.die-interaktiven.de'});
            msg.setPlainTextBody('finished errMessage: ' + errMessage);
            Messaging.sendEmail(new List<Messaging.Singleemailmessage>{msg});
        }
        
    }
    
}