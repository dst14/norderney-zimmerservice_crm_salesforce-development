@isTest
public class NZITestDataFactory {
    
 /**
 * NorderneyTestDataFactory.cls
 * Creates generic organisation-specific test data
 * 
 * @author:             Daniel Stange <daniel.stange@die-interaktiven.de>
 * 
 * @version history:    0.0.1   2016-04-27  Initial 
 * 
 * 
 * @properties
 * 
 * + List<Account>                      allAccounts;
 * + List<Contact>                      allContacts;
 * + List<Asset>                        allAssets;
 * 
 * @methods
 * 
 * + void createC
 */ 

    public static List<Account>                     allAccounts;
    public static List<Contact>                     allContacts;
    public static List<AhisObject__c>               allObjects;
    public static List<Property__c>                 allProperties;
    public static List<Cleaner__c>                  allCleaners;
    
    public static Set<String> objectsToCreate = new Set<String>();
    
    public static void loadStaticTestData() {
        /*
         * Method to load static test data
         * 
        List<sObject> ls = Test.loadData(CountryMapping__c.sObjectType, 'TestData_CountryMapping');
        System.debug(loggingLevel.INFO,'!!! Number of CountryMapping__c records created:'+ls.size());
        */
    }
    
    public static void createAccountsAndContacts() {
        
        List<Account> accounts = new List<Account>();
        Account acct;
        
        for (integer count=0;count<10;count++) {
        
            acct = new Account();
            acct.Name='TestAccount '+count;
            accounts.add(acct);
        }
        
        insert accounts;
        NZITestDataFactory.allAccounts = accounts;
        
        List<Contact> contacts = new List<Contact>();
        Contact cont;
        
        for (integer count=0;count<10;count++) {
        
            cont = new Contact();
            cont.LastName  ='TestNachname '+count;
            cont.AccountId = accounts[count].Id; 
            contacts.add(cont);
        }
        
        insert contacts;
        NZITestDataFactory.allContacts = contacts;
    }
    
    public static void createCases() {
        
        List<Case> cases = new List<Case>();
        Case cs;
        
        for (integer count=0;count<10;count++) {
        
            cs = new Case();
            cs.Origin   = 'Internet';
            cs.Status   = 'New';
            cs.Subject  = 'Testcase 1';
            
            cases.add(cs);
        }
        
        insert cases;
    }
    
    public static void createOpportunities() {
        
        List<Opportunity> oppts = new List<Opportunity>();
        Opportunity oppt;
        
        for (integer count=0;count<10;count++) {
        
            oppt = new Opportunity();
            oppt.Name= 'TestOpportunity '+count;
            oppt.StageName = 'Interessiert';
            oppt.CloseDate = date.today().addDays(14);
            oppt.AccountId = NZITestDataFactory.allAccounts[1].Id;
            oppts.add(oppt);
        }
        
        insert oppts;
    }

    public static void createRealEstate() {
        
        List<AhisObject__c> obj = new List<AhisObject__c>();
        AhisObject__c o;
        
        for (integer count=0;count<10;count++) {
        
            o = new AhisObject__c();
            o.Name= 'RealEstate '+ String.valueOf(count);
            o.ObjectStreet__c = 'Teststraße ' + String.valueOf(1 + count);
            o.ObjectPostalCode__c = String.valueOf(20000 + count);
            o.ObjectCity__c = 'Norderney';
            obj.add(o);
        }
        
        insert obj;

        NZITestDataFactory.allObjects = obj;
    }

    public static void createProperties() {
        
        List<Property__c> prop = new List<Property__c>();
        Property__c p;
        
        for (integer count=0;count<10;count++) {
        
            p = new Property__c();
            p.Name= 'Property '+ String.valueOf(count);
            p.AhisObject__c = NZITestDataFactory.allObjects[count].Id;
            prop.add(p);
        }
        
        insert prop;

        NZITestDataFactory.allProperties = prop;
    }

    public static void createCleaners() {
        
        List<Cleaner__c> cleaners = new List<Cleaner__c>();
        Cleaner__c cl;
        
        for (integer count=0;count<10;count++) {
        
            cl = new Cleaner__c(
                Name= 'Cleaner '+ String.valueOf(count),
                isActive__c = true
            );
            cleaners.add(cl);
        }
        
        insert cleaners;

        NZITestDataFactory.allCleaners = cleaners;
    }
    
    public static testMethod void testCreateData() {
        
        loadStaticTestData();
        createAccountsAndContacts();
        
        System.assert(0<[Select count() from Account]);
        System.assert(0<[Select count() from Contact]);
        
        if (objectsToCreate.isEmpty() || objectsToCreate.contains('Opportunity')) {
            createOpportunities();
            System.assert(0<[Select count() from Opportunity]);            
        }
        
        if (objectsToCreate.isEmpty() || objectsToCreate.contains('Case')) {
            createCases();
            System.assert(0<[Select count() from Case]);
        }

        // Property__c depends on AhisObject__c
        if (objectsToCreate.isEmpty() || objectsToCreate.contains('AhisObject__c') || objectsToCreate.contains('Property__c')) {
            createRealEstate();
            System.assert(0<[Select count() from AhisObject__c]);
        }

        if (objectsToCreate.isEmpty() || objectsToCreate.contains('Property__c')) {
            createProperties();
            System.assert(0<[Select count() from Property__c]);
        }

        if (objectsToCreate.isEmpty() || objectsToCreate.contains('Cleaner__c')) {
            createCleaners();
            System.assert(0<[Select count() from Cleaner__c]);
        }        
    }
}