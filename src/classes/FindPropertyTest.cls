@isTest 
private class FindPropertyTest {

	static testMethod void FindPropertyTest() {
		
        Property__c property = new Property__c(name = 'property1');
        insert property;
        
        Case c = new Case(Status = 'neu', Origin = 'Email');
        insert c;
        
        ApexPages.StandardController stdCont = new ApexPages.StandardController(c);
        
        Test.startTest();
        
        FindPropertyCont cont = new FindPropertyCont(stdCont);
        cont.searchInput = 'p';
        cont.searchProperties();
        cont.searchInput = 'x';
        cont.searchProperties();
        
        Test.stopTest();
        
	}
}